% oLaF - a flexible 3D reconstruction framework for light field microscopy
% Copyright (c)2017-2020 Anca Stefanoiu and Josue Page

% this script serves as a quick demo of the LFM functionality in oLaF v3.0
% the sample datasets are found in ../../SampleData/LFM/

%% Import dependecies
import2ws();

%% Choose raw image to reconstruct and correponding white image (to identify the mlens centers) +  axial range and resolution
% dataset = 'organoid';
% depthRange = [0, 30]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

% dataset = 'fishEye';
% depthRange = [-20, 20]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

% dataset = 'spheres';
% depthRange = [-20, 20]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

% dataset = 'spheresDefocusedDown'; % defocused LFM
% depthRange = [-55, -15]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

% dataset = 'spheresDefocusedUp'; % defocused LFM 
% depthRange = [5, 45]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

dataset = 'fishMulti'; % multifocus
depthRange = [-15, 5]; %reconstruction depth range (im um)
depthStep = 5; % axial resolution (in um)

% dataset = 'USAF'; % multifocus
% depthRange = [-15, 5]; %reconstruction depth range (im um)
% depthStep = 5; % axial resolution (in um)

[LensletImage, WhiteImage, configFile] = LFM_selectImages(dataset);
figure; imagesc(LensletImage); colormap inferno; title ('LF image'); drawnow

%% Input parameters 
% choose lenslet spacing (in  pixels) to downsample the number of pixels between mlens for speed up
newSpacingPx = 15; % 'default' means no up/down-sampling (newSpacingPx = lensPitch/pixelPitch)
% choose super-resolution factor as a multiple of lenslet resolution (= 1 voxel/lenslet)
superResFactor = 'default'; % default means sensor resolution

%% Specific LFM configuration and camera parameters (um units)
Camera = LFM_setCameraParams(configFile, newSpacingPx);

%% Compute LFPSF Patterns and other prerequisites: lenslets centers, resolution related
[LensletCenters, Resolution, LensletGridModel, NewLensletGridModel] = ...
                LFM_computeGeometryParameters(Camera, WhiteImage, depthRange, depthStep, superResFactor, 1);
                                         
[H, Ht] = LFM_computeLFMatrixOperators(Camera, Resolution, LensletCenters);

%% Correct the input image 
% obtain the transformation between grid models
FixAll = LFM_retrieveTransformation(LensletGridModel, NewLensletGridModel);

% apply the transformation to the lenslet and white images
[correctedLensletImage, correctedWhiteImage] = LFM_applyTransformation(LensletImage, WhiteImage, FixAll, LensletCenters, 1);
correctedLensletImage(correctedLensletImage < mean(correctedLensletImage(:))) = mean(correctedLensletImage(:));
correctedLensletImage = mat2gray(single(correctedLensletImage));

%% Reconstruct

% precompute image/volume sizes
imgSize = size(correctedLensletImage);
imgSize = imgSize + (1-mod(imgSize,2)); % ensure odd size

texSize = ceil(imgSize.*Resolution.texScaleFactor);
texSize = texSize + (1-mod(texSize,2)); % ensure odd size

% Setup function pointers
if (strcmp(Camera.focus, 'single'))
    backwardFUN = @(projection) LFM_backwardProject(Ht, projection, LensletCenters, Resolution, texSize, Camera.range);
    forwardFUN = @(object) LFM_forwardProject(H, object, LensletCenters, Resolution, imgSize, Camera.range);
elseif (strcmp(Camera.focus, 'multi'))
    backwardFUN = @(projection) LFM_backwardProjectMultiFocus(Ht, projection, LensletCenters, Resolution, texSize, Camera.range);
    forwardFUN = @(object) LFM_forwardProjectMultiFocus(H, object, LensletCenters, Resolution, imgSize, Camera.range);
else
    error('Invalid micro-lens type.')
end

% build anti-aliasing filter kernels
lanczosWindowSize = 2;
widths = LFM_computeDepthAdaptiveWidth(Camera, Resolution);
lanczos2FFT = LFM_buildAntiAliasingFilter([texSize, length(Resolution.depths)], widths, lanczosWindowSize);

% Apply EMS deconvolution
it = 3;

% initialization
initVolume = ones([texSize, length(Resolution.depths)]);
LFimage = correctedLensletImage;

% background correction
onesvol = ones(size(initVolume));
onesForward = forwardFUN(onesvol);
onesBack = backwardFUN(onesForward);

reconVolume = deconvEMS(forwardFUN, backwardFUN, LFimage, it, initVolume, 1, lanczos2FFT, onesForward, onesBack);

%% Display the reconstruction
figure;
if(size(reconVolume, 3) > 1)
    imshow3D(reconVolume, [], 'inferno');
else
    imagesc(reconVolume); colormap inferno
end