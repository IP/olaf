% oLaF - a flexible 3D reconstruction framework for light field microscopy
% Copyright (c)2017-2020 Anca Stefanoiu

% this script serves as a quick demo of the Fourier LFM functionality in oLaF v3.0
% the sample datasets are found in ../../SampleData/FLFM/

%% Import dependecies
import2ws();

%% Choose a raw image to reconstruct and the correponding calibration image (to identify the mlens centers)

%------------3D---------- cotton fiber image
LensletImage = imread(['../../SampleData/FLFM/Fibers/', 'LFImage.png']); 
CalibrationImage = imread(['../../SampleData/FLFM/Fibers/', 'CalibrationImage.png']); 
configFile = '../../SampleData/FLFM/Fibers/LFMconfig.yaml';

depthRange = [-80, 80]; %reconstruction depth range (im um)
depthStep = 20; % axial resolution (in um)

%------------2D---------- USAF resolution target 
% LensletImage = imread(['../../SampleData/FLFM/USAF_zstack/', 't-100.bmp']); 
% CalibrationImage = imread(['../../SampleData/FLFM/USAF_zstack/', 'CalibrationImage.bmp']); 
% configFile = '../../SampleData/FLFM/USAF_zstack/LFMconfig.yaml';
% 
% depthRange = [-100, -100]; %reconstruction depth range (im um) (for 2D objects depthRange start and end are the same)
% depthStep = 20; % axial resolution (in um) % dummy value for 2D objects

%% Input parameters 
% choose super-resolution factor as a multiple of sub-aperture image resolution (sensor resolution)
superResFactor = 1; % 1 means sensor resolution

% upscale the image according to superResFactor
LensletImage = double(rgb2gray(im2uint16(LensletImage)));
LensletImage = imresize(LensletImage, superResFactor, 'nearest');
LensletImage = makeOddSize(LensletImage); % make sure the image size is odd, the recon assumes a central pixel (on the optical axis)

% a calibration image is taken when an object (usually a resolution target) is
% placed at the native object plane, such that the centers of the
% sub-aperture images coincide with the centers of the lenslets
CalibrationImage = double(rgb2gray(im2uint16(CalibrationImage)));
CalibrationImage = imresize(CalibrationImage, superResFactor, 'nearest');
CalibrationImage = makeOddSize(CalibrationImage); % make sure the image size is odd, the recon assumes a central pixel (on the optical axis)

% display the raw LF image
figure; imagesc(LensletImage); colormap inferno; title ('LF image'); drawnow

%% Specific FLFM configuration and camera parameters (um units)
[Camera, LensletGridModel] = FLFM_setCameraParams(configFile, superResFactor);

%% Compute LFPSF and other prerequisites: lenslets centers, resolution related and rectify the lenslet image
[LensletCenters, Resolution] = FLFM_computeGeometryParameters(CalibrationImage, Camera, LensletGridModel, depthRange, depthStep);

% when necessarily rectify the lenslet image so that only the whole elemental images are
% kept to ensure all points in the scene are consistently measured (necessary for the USAF image)
MLAmask = FLFM_grabFullLens(Camera, Resolution);
LensletImage = LensletImage.*MLAmask;

[H, Ht] = FLFM_computeLFMatrixOperators(Camera, Resolution);

%% Reconstruct
% set up forward/backward operators function pointers
forwardFUN = @(volume) FLFM_forwardProject(H, volume);
backwardFUN = @(projection) FLFM_backwardProject(Ht, projection);
    
% solution initialization
volumeSize = [size(LensletImage,1), size(LensletImage,2), size(Ht,3)];
init = ones(volumeSize); 

% Richardson Lucy deconvolution
iter = 10; % number of iterations
recon = deconvRL(forwardFUN, backwardFUN, LensletImage, iter, init);

% One step late deconvolution (with TV regularization)
% iter = 10; % number of iterations
% lambda = 0.02; % regularization parameter
% recon = deconvOSL(forwardFUN, backwardFUN, LensletImage, iter, init, lambda);

%% Display the reconstruction
% crop to the field of view of the microscope
fovRangeY = (ceil(size(recon,1)/2) - Resolution.fovRadVox(1) : ceil(size(recon,1)/2) + Resolution.fovRadVox(1)); 
fovRangeX = (ceil(size(recon,2)/2) - Resolution.fovRadVox(2) : ceil(size(recon,2)/2) + Resolution.fovRadVox(2)); 
reconCropped = recon(fovRangeY, fovRangeX, :);

figure;
if(size(reconCropped, 3) > 1)
    imshow3D(reconCropped, [], 'inferno');
else
    imagesc(reconCropped); colormap inferno
end