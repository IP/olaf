% oLaF - a flexible 3D reconstruction framework for light field microscopy
% Copyright (c)2017-2020 Anca Stefanoiu

function [H, Ht] = FLFM_computeLFMatrixOperators(Camera, Resolution)

%% Sensor and ML space coordinates
IMGsizeHalf_y = floor(Resolution.sensorSize(1)/2);
IMGsizeHalf_x = floor(Resolution.sensorSize(2)/2);
Resolution.yspace = Resolution.sensorRes(1)*linspace(-IMGsizeHalf_y, IMGsizeHalf_y, Resolution.sensorSize(1));
Resolution.xspace = Resolution.sensorRes(2)*linspace(-IMGsizeHalf_x, IMGsizeHalf_x, Resolution.sensorSize(2));
Resolution.yMLspace = Resolution.sensorRes(1)* [- ceil(Resolution.Nnum(1)/2) + 1 : 1 : ceil(Resolution.Nnum(1)/2) - 1];
Resolution.xMLspace = Resolution.sensorRes(2)* [- ceil(Resolution.Nnum(2)/2) + 1 : 1 : ceil(Resolution.Nnum(2)/2) - 1];

%% Compute LFPSF operators

% compute the wavefront distribution incident on the MLA for every depth
fprintf('\nCompute the PSF stack at the back aperture stop of the MO.')
psfSTACK = FLFM_calcPSFAllDepths(Camera, Resolution);

% compute LFPSF at the sensor plane
fprintf('\nCompute the LFPSF stack at the camera plane:')
tolLFpsf = 0.05; % clap small values in the psf to speed up computations
[H, Ht] = FLFM_computeLFPSF(psfSTACK, Camera, Resolution, tolLFpsf);