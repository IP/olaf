% oLaF - a flexible 3D reconstruction framework for light field microscopy
% Copyright (c)2017-2020 Anca Stefanoiu 

function MLARRAY = FLFM_mlaTransmittance(Resolution, ulensPattern)

%% Compute the ML array as a grid of phase/amplitude masks corresponding to mlens
ylength = length(Resolution.yspace);
xlength = length(Resolution.xspace);

LensletCenters(:,:,1) = round(Resolution.LensletCenters(:,:,2));
LensletCenters(:,:,2) = round(Resolution.LensletCenters(:,:,1));

% activate lenslet centers -> set to 1
MLcenters = zeros(ylength, xlength);
for a = 1:size(LensletCenters,1) 
    for b = 1:size(LensletCenters,2) 
        if( (LensletCenters(a,b,1) < size(ulensPattern,1)/2) || (LensletCenters(a,b,1) > ylength - size(ulensPattern,1)/2) || ...
              (LensletCenters(a,b,2)< size(ulensPattern,2)/2) || (LensletCenters(a,b,2) > xlength - size(ulensPattern,2)/2)  )
          continue
        end
        MLcenters( LensletCenters(a,b,1), LensletCenters(a,b,2)) = 1;
    end
end

% apply the mlens pattern at every ml center
MLARRAY  = conv2(MLcenters, ulensPattern, 'same');